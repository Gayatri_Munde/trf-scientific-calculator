#include <avr/io.h>
//header to enable data flow control over pins

#define F_CPU 16000000      
//telling controller crystal frequency attached

#include <util/delay.h>
#define A PB0
#define B PB1
#define C PB2
#define D PB3
#define a PB4
#define b PB5
#define c PB6
#define d PB7
#define E PC5
#define RS PC4

int main(){
  DDRD=0xFF;//PORTD-output
  DDRC|=(1<<E);//PORT B pin 0 -enable -output
  DDRC|=(1<<RS);//PORT B PIN 1 -REGISTER SELECT- OUTPUT
  
  PORTD = 0x38;//INTITIALIZE AS 7*5 MATRIX
  command_enable();
  PORTD = 0x0E;//Turn on LCD, cursor display 
  command_enable();
  PORTD = 0x01;//clear data
  command_enable();
  PORTD = 0x80;//R=1ST C=1ST IS CHOSEN
  command_enable();
  
  DDRB = 0xFF;//PORTB-INPUT
  PORTB = 0x00;//ALL OUTPUT PORTB AS LOW

  char key[4][4]={
    {'7','8','9','/'},
    {'4','5','6','*'},
    {'1','2','3','-'},
    {'n','0','=','+'}
    };
  
   uint8_t i, j, row_check = 0;

  
  while(1) {
  
  PORTB |= (1<<A) | (1<<B) | (1<<C) | (1<<D);
  if(PINB & (1<<a))
  {
    i = 0;
    row_check = 1;
  }
  else if(PINB & (1<<b))
  {
    i = 1;
    row_check = 1;
  }
    else if(PINB & (1<<c))
  {
    i = 2;
    row_check = 1;
  } 
  else if(PINB & (1<<d))
  {
    i = 3;
    row_check = 1; 
  }
  _delay_us(100);
    PORTB=0x00;
  if (row_check == 1)
  { 
  
    PORTB |= (1<<a)|(1<<b)|(1<<c)|(1<<d);
    if(PINB & (1<<A))
    {
      j = 0;
    }
    else if(PINB & (1<<B))
    {
      j = 1;
    }
    else if(PINB & (1<<C))
    {
      j = 2;
    } 
    else if(PINB & (1<<D))
    { 
      j = 3;
    }

    row_check = 0;
    _delay_us(100);
    PORTB = 0x00;
    _delay_us(100);

    if (key[j][i] == 'n') //works as backspace
    {
      PORTD = 0x01;//clear data
      command_enable();
      }
    else{
    PORTD = (unsigned char) key[j][i];
    data_enable();
    }
   }
  }
 return 0;
}

void command_enable()
{
  PORTC &=~ (1<<RS);//Register Select=0
  PORTC |= (1<<E);//Enable high to low
  _delay_ms(100);
  PORTC &=~ (1<<E);
}


void data_enable()
{
  PORTC |= (1<<RS);
  PORTC |= (1<<E);
  _delay_ms(100);
  PORTC &=~ (1<<E); 
}
