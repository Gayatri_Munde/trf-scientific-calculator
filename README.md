Designing **Scientific calculator** on atmega328p microcontroller using keypad as input and display as output<br>
- **Steps we followed**:
1. Interfaced the 16X2 LCD display with atmega328p
2. Interfaced the Keypad and LCD with atmega328p

- **Softwares we used**
1. Proteus
2. Arduino
