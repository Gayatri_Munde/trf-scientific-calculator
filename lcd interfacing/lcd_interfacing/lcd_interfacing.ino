#include<avr/io.h>
#include<util/delay.h>
void command_enable();
void data_enable();
int main()
{
  DDRD = 0x0FF;
  DDRB |= (1<<PB0);
  DDRB |= (1<<PB1);
  PORTD = 0x38;
  command_enable();
  PORTD = 0x0E;
  command_enable();
  PORTD = 0x01;
  command_enable();
  PORTD = 0x80;
  command_enable();
  unsigned char name[5]={'P','U','R','V','A'};
  int i;
  for(i=0;i<5;i++)
  {
    PORTD = name[i] ;
    data_enable();
  }
  while(1);
  return 0;
}

void command_enable()
{
  PORTB&=~(1<<PB1);//Register Select=0
  PORTB|=(1<<PB0);//Enable high to low
  _delay_ms(10);
  PORTB&=~(1<<PB0);
}
void data_enable()
{
  PORTB|=(1<<PB1);
  PORTB|=(1<<PB0);
  _delay_ms(10);
  PORTB&=~(1<<PB0);
}
  
  
