#include<avr/io.h>
#include<util/delay.h>
void command_enable();
void data_enable();
int main()
{
  DDRD=0xFF;//PORTD-output
  DDRB|=(1<<PB0);//PORT B pin 0 -enable -output
  DDRB|=(1<<PB1);//PORT B PIN 1 -REGISTER SELECT- OUTPUT
  PORTD=0x38;//INTITIALIZE AS 7*5 MATRIX
  command_enable();
  PORTD=0x0E;//Turn on LCD,cursor display 
  command_enable();
  PORTD=0x01;//clear data
  command_enable();
  PORTD=0x80;//R=1ST C=1ST IS CHOSEN
  command_enable();
  PORTD=(unsigned char)'a';
  data_enable();
  while(1);
 
  return 0;
}
void command_enable()
{
  PORTB&=!(1<<PB1);//Register Select=0
  PORTB|=(1<<PB0);//Enable high to low
  _delay_ms(10);
  PORTB&=~(1<<PB0);
}
void data_enable()
{
  PORTB|=(1<<PB1);
  PORTB|=(1<<PB0);
  _delay_ms(10);
  PORTB&=~(1<<PB0);
}
